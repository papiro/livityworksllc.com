'use strict'

const 
  express = require('express'),
  app = express(),
  port = 5555
,
  cfg = require('./config'),
  { views, testimonials } = cfg,
  rData = views.reduce( (map, { route, label }) => {
    map[route] = {
      label
    } 
    return map
  }, {})
,
  buildTokens = (req) => {
    return Object.assign({
      route: rData[req.url],
      tabs: views,
      isHome: req.url === '/'
    }, req.url === '/testimonials' && {
      testimonials  
    })
  }
;

app.set('view engine', 'pug')

app.use((req, res, next) => {
  console.log(req.url)
  next()
})

views.forEach( ({ route, view }) => {
  app.get(route, (req, res) => {
    res.render(view, buildTokens(req))
  })
})

app.listen(port)
