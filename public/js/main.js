function main () {
  const 
    nav = document.getElementsByTagName('nav')[0],
    cl = nav.classList,
    refmap = buildRefMap(nav)
  ;

  document.addEventListener('click', evt => {
    if (!~refmap.indexOf(evt.target))
      closeNav()
  })
  document.getElementById('menu').addEventListener('click', evt => {
    evt.stopPropagation()
    if (cl.contains('show'))
      closeNav()
    else
      cl.add('show')
  })

  function closeNav () {
    cl.remove('show')
  }
  function buildRefMap (el) {
    return Array.from(el.children).reduce( (arr, child) => {
      arr.push(child) 
      arr.concat(buildRefMap(child))
      return arr
    }, [])
  }
}

main()
