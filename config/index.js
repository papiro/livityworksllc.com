'use strict'

exports.testimonials = require('./testimonials').testimonials

exports.views = [
  { 
    "route": "/",
    "view": "index",
    "label": "Home"
  },
  {
    "route": "/tree-care",
    "view": "tree_care",
    "label": "Tree Care"
  },
  {
    "route": "/design",
    "view": "gardening",
    "label": "Design"
  },
  {
    "route": "/international-tree-climbing",
    "view": "intl_tree_climbing",
    "label": "International Tree Climbing"
  },
  {
    "route": "/about",
    "view": "about",
    "label": "About"
  },
  {
    "route": "/philosophy",
    "view": "philosophy",
    "label": "Philosophy"
  },
  {
    "route": "/bio",
    "view": "bio",
    "label": "Bio"
  },
  {
    "route": "/gallery",
    "view": "gallery",
    "label": "Gallery"
  },
  {
    "route": "/testimonials",
    "view": "testimonials",
    "label": "Testimonials"
  },
  {
    "route": "/contact",
    "view": "contact",
    "label": "Contact"
  }
]
