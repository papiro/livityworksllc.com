'use strict'

const 
  path = require('path'), fs = require('fs'), vm = require('vm'),
  express = require('express'),
  app = express()
;

app.get('*', (req, res) => {
  res.render('index', { isProd: process.env.NODE_ENV === 'production' })
})

app.listen(5555, () => {
  console.log('livityworksllc.com being served')
})

app.engine('bns', (filePath, options, callback) => {
  const tag = 'bean'

  let
    sandbox = Object.assign({ path, fs, console, env: process.env }, options),
    sandboxOptions = { displayErrors: true }
  ;

  // now onto business
  fs.readFile(filePath, { encoding: 'utf8' }, (err, markup) => {
    if (err) {
      return callback(err)
    }

    if (~markup.indexOf(`<${tag}>`)) {
      const tagMatcher = new RegExp(`<${tag}>[\\s\\S]*?<\/${tag}>`, 'g')
      const scriptMatcher = new RegExp(`(?:<${tag}>)([\\s\\S]*)(?:<\/${tag}>)`)
      let rendered = markup
      let cyclic = []
      while ((cyclic = tagMatcher.exec(markup)) !== null) {
      // while ((cyclic = scriptMatcher.exec(markup)) !== null) {
        const script = cyclic[0].match(scriptMatcher)[1]
        const preppedScript = 
        // wrap vm'd script in IIFE to disconnect it from calling globals, thereby
        // significantly improving performance (beyond even that of eval)
        `
        (() => { ${script} } )()
        `
        const output = vm.runInNewContext(preppedScript, sandbox, sandboxOptions)
        rendered = rendered.replace(cyclic[0], output)          
      }
      return callback(null, rendered)
    }
  })
})
app.set('views', './public/templates')
app.set('view engine', 'bns')
