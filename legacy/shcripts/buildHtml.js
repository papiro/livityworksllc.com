#!/usr/local/bin/node
'use strict'

var fs = require('fs')

fs.readFile('src/dev.html', 'utf8', function(err, data) {
  if (err) return console.log(err)
  data = data.replace(RegExp('<!--srcBegin-->[\\s\\S]*<!--srcEnd-->|<!--distBegin|distEnd-->', 'g'), '')
  fs.writeFile('src/index.html', data, 'utf8', function(err, data) {
    if (err) return console.log(err)
  })  
})
