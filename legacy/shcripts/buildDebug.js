#!/usr/local/bin/node
'use strict'

var fs = require('fs')

fs.readFile('src/index.html', 'utf8', function(err, data) {
  fs.writeFile('src/debug.html', data.replace(RegExp('<!--debug|debug-->', 'g'), ''), 'utf8', function(err) {
    if(err) console.error(err)
  })
})
