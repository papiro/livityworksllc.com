'use strict'

;(function () {
  var livityworks = angular.module('livityworks', ['ngRoute'])

  livityworks.config(['$compileProvider', '$routeProvider', '$locationProvider', function ($compileProvider, $routeProvider, $locationProvider) {
    function t (route) { return '/templates/' + route }
    $routeProvider
      .when('/', {
        templateUrl: t('home.html'),
        controller: function () {}
      })
      .when('/about', {
        templateUrl: t('about.html'),
        controller: function () {}
      })
      .when('/bio', {
        templateUrl: t('bio.html'),
        controller: function () {}
      })
      .when('/philosophy', {
        templateUrl: t('philosophy.html'),
        controller: function () {}
      })
      .when('/intl-tree-climbing', {
        templateUrl: t('intl-tree-climbing.html'),
        controller: function () {}
      })
      .when('/gardening', {
        templateUrl: t('gardening.html'),
        controller: function () {}
      })
      .when('/tree-care', {
        templateUrl: t('tree-care.html'),
        controller: function () {}
      })
      .when('/dj-ing', {
        templateUrl: t('dj-ing.html'),
        controller: function () {}
      })
      .when('/contact', {
        templateUrl: t('contact.html'),
        controller: function () {}
      })
      .when('/gallery', {
        templateUrl: t('gallery.html'),
        controller: function () {}
      })
      .when('/testimonials', {
        templateUrl: t('testimonials.html'),
        controller: function () {}
      })
      .when('/spiral-garden-1', {
        templateUrl: t('testimonials.html'),
        controller: function () {}
      })
      .otherwise({
        templateUrl: t('404.html'),
        controller: function () {}
      })
    $compileProvider.debugInfoEnabled(window.ENV !== 'prod')
    $compileProvider.commentDirectivesEnabled(false)
    $compileProvider.cssClassDirectivesEnabled(false)
    // $locationProvider.hashPrefix('')
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    })
  }])

  livityworks.controller('view-ctrl', ['$scope', '$location', '$route', function ($scope, $location, $route) {

  }])
  livityworks.controller('header-ctrl', ['$scope', function ($scope) {
    
  }])
  livityworks.controller('nav-ctrl', ['$scope', function ($scope) {
    $scope.$on('$routeChangeSuccess', function(scope, next, current) {
      var target = next.originalPath.replace('/', '')
      angular.element(document.querySelectorAll('.navbar-nav li')).removeClass('active')
      angular.element(document.querySelector('.tab-group [href="'+(target ? target : '/')+'"]')).parent().addClass('active')
    })
    $scope.tabsGroup1 = [
    { label: 'Home', target: '/' },
    { label: 'About', target: 'about' },
    { label: 'Bio', target: 'bio' },
    { label: 'Philosophy', target: 'philosophy' },
    { label: 'International Tree Climbing', target: 'intl-tree-climbing' },
    { label: 'Gardening', target: 'gardening' },
    { label: 'Tree Care', target: 'tree-care' },
    { label: 'DJing', target: 'dj-ing' },
    { label: 'Contact', target: 'contact' },
    { label: 'Gallery', target: 'gallery' },
    { label: 'Testimonials', target: 'testimonials' }
    ]

    $scope.cleanup = function () {
      $('[ng-repeat]').removeAttr('ng-repeat on-render')
    }
  }])

  livityworks.directive('onRender', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        if (scope.$last) {
          scope.$evalAsync(attrs.onRender + '()') 
        }
      }
    }
  })
})()
